# Unreal 5 Warehouse Wreckage

My first small game created with Unreal Engine 5 for the "Unreal 5.0 C++ Developer: Learn C++ and Make Video Games" course from GameDev.tv.
Using the "Industry Props Pack 6" free assets from the Unreal Engine Marketplace.
